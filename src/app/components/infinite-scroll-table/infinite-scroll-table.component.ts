import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { SweetalertService } from 'src/app/service/sweetalert.service';
import { GlobalconstantsService } from 'src/app/service/globalconstants.service';
import { ApiConfig } from 'src/app/constants/api-config';
import { User } from 'src/app/model/User';

@Component({
  selector: 'app-infinite-scroll-table',
  templateUrl: './infinite-scroll-table.component.html',
  styleUrls: ['./infinite-scroll-table.component.css']
})
export class InfiniteScrollTableComponent implements OnInit {

  public userDataList: User[ ] = [];
  
  constructor(private apiService: ApiService,
    private sweetAlertService: SweetalertService,
    private globalConstants: GlobalconstantsService) { }

  ngOnInit(): void {
    this.getSampleUserDataList();
  }
  getSampleUserDataList(){
    this.apiService.sendGetRequest(ApiConfig.SAMPLE_URL).subscribe(res => {
      for(let i = 0; i < res.length; i++){
        this.userDataList.push(res[i]) ;
      }
    },
    (error) => {
      if (error.error instanceof ErrorEvent) {
      } 
      else  
      {
          const displayMessage = this.apiService.getErrorMessage(error);
          const displayErrorType = this.apiService.getErrorType(error);
          this.sweetAlertService.Error(displayErrorType, displayMessage, this.globalConstants.SWAL_ERROR_ICON);
      }
    });
  }
  submit(id, status){
    const msg = 'Row id ' + " ' " + id + " ' " + " and Status " + " ' " + status + " ' " + ' saved successfully!! ';
    this.sweetAlertService.Success(this.globalConstants.SUCCESS, msg, this.globalConstants.SWAL_SUCCESS_ICON);
  }

  onScroll() {
    this.getSampleUserDataList();
  }
}
