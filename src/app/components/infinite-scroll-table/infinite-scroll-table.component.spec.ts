import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfiniteScrollTableComponent } from './infinite-scroll-table.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ApiService } from 'src/app/service/api.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

describe('InfiniteScrollTableComponent', () => {
  let component: InfiniteScrollTableComponent;
  let fixture: ComponentFixture<InfiniteScrollTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfiniteScrollTableComponent ],
      imports: [
        HttpClientTestingModule, InfiniteScrollModule
      ],
      providers: [
        ApiService
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfiniteScrollTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
