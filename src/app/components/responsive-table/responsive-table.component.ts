import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/User';
import { ApiService } from 'src/app/service/api.service';
import { ApiConfig } from 'src/app/constants/api-config';
import { SweetalertService } from 'src/app/service/sweetalert.service';
import { GlobalconstantsService } from 'src/app/service/globalconstants.service';


@Component({
  selector: 'app-responsive-table',
  templateUrl: './responsive-table.component.html',
  styleUrls: ['./responsive-table.component.css']
})
export class ResponsiveTableComponent implements OnInit {

  public userDataList: User[ ];
  public limit = 10;
  public itemsPerPage = 10;
  public maxSize = 10;
  public numPages = 1;
  p = 1;
  constructor(private apiService: ApiService,
    private sweetAlertService: SweetalertService,
    private globalConstants: GlobalconstantsService) { }

  ngOnInit(): void {
    this.getSampleUserDataList();
  }
  getSampleUserDataList(){
    this.apiService.sendGetRequest(ApiConfig.SAMPLE_URL).subscribe(res => {
      this.userDataList = res;
      this.sweetAlertService.Success(this.globalConstants.SUCCESS, this.globalConstants.BLANK_STRING, 
        this.globalConstants.SWAL_SUCCESS_ICON);
    },
    (error) => {
      if (error.error instanceof ErrorEvent) {
      } 
      else   
      {
          const displayMessage = this.apiService.getErrorMessage(error);
          const displayErrorType = this.apiService.getErrorType(error);
          this.sweetAlertService.Error(displayErrorType, displayMessage, this.globalConstants.SWAL_ERROR_ICON);
      }
    });
  }
  submit(id, status){
    const msg = 'Row id ' + " ' " + id + " ' " + " and Status " + " ' " + status + " ' " + ' saved successfully!! ';
    this.sweetAlertService.Success(this.globalConstants.SUCCESS, msg, this.globalConstants.SWAL_SUCCESS_ICON);
  }
}
