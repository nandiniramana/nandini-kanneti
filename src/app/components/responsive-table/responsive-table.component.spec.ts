import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsiveTableComponent } from './responsive-table.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ApiService } from 'src/app/service/api.service';
import { NgxPaginationModule } from 'ngx-pagination';

describe('ResponsiveTableComponent', () => {
  let component: ResponsiveTableComponent;
  let fixture: ComponentFixture<ResponsiveTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponsiveTableComponent ],
      imports: [
        HttpClientTestingModule, NgxPaginationModule
      ],
      providers: [
        ApiService,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsiveTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
