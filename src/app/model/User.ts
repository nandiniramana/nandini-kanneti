export class User{
    id: number;
    name: string;
    phone: string;
    email: string;
    company: string;
    date_entry: string;
    org_num: string;
    address_1: string;
    city: string;
    zip: string;
    geo: string;
    pan: string;
    pin: string;
    status: string;
    fee: string;
    guid: string;
    date_exit: string;
    date_first: string;
    date_recent: string;
    url: string;

    constructor(){
        this.id = 0;
        this.name = '';
        this.phone = '';
        this.email = '';
        this.company = '';
        this.date_entry = '';
        this.org_num = '';
        this.address_1 = '';
        this.city = '';
        this.zip = '';
        this.geo = '';
        this.pan = '';
        this.pin = '';
        this.status = '';
        this.fee = '';
        this.guid = '';
        this.date_exit = '';
        this.date_first = '';
        this.date_recent = '';
        this.url = '';
    }
}
