import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResponsiveTableComponent } from './components/responsive-table/responsive-table.component';
import { InfiniteScrollTableComponent } from './components/infinite-scroll-table/infinite-scroll-table.component';


const routes: Routes = [

  { path: '', component: ResponsiveTableComponent },
  { path: 'responsive-table', component: ResponsiveTableComponent },
  { path: 'infinite-scroll-table', component: InfiniteScrollTableComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
