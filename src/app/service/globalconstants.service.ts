import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalconstantsService {

  constructor() { }
  SUCCESS = 'Success';
  BLANK_STRING = '';
  SUBMIT = 'Row id and status submitted successfully!';

  SWAL_SUCCESS_ICON = 'success';
  SWAL_ERROR_ICON = 'error';
  SWAL_WARNING_ICON = 'warning';
  SWAL_INFO_ICON = 'info';
}
