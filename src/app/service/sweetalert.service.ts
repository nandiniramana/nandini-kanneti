import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js';
@Injectable({
  providedIn: 'root'
})
export class SweetalertService {

  constructor() { }
  Success(title: string, message: string, icon: string)
  {
    Swal.fire(title, message, icon );
  }

  Error(errorType: string, errorMessage: string, icon: string)
  {
    Swal.fire(errorType, errorMessage, icon);
  }

  Warning(errorType: string, errorMessage: string, icon: string)
  {
    Swal.fire(errorType, errorMessage, icon);
  }
}
