import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/User';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  public sendGetRequest(apiUrl: string, headers?: {[key: string]: string}): Observable<any>{
    return this.get(apiUrl, this.setHeaders(headers))
    .map((res: any) => {
      try{
        return res;
      }catch ( err ){
        return { };
      }
    }).catch(this.handleError);
  }
  get(url: string, options?: any): Observable<any>{
    return this.http.get(url);
  }

  // Methods to add custom headers if any
  private setHeaders(extraHeaders?: {[key: string]: string}): any{
    const headers = new HttpHeaders();

    if ( extraHeaders ) {
      for ( const key of Object.keys( extraHeaders ) ) {
        headers.append(key, extraHeaders[key]);
      }
    }
    const requestOpts: any = { headers : headers };
    return requestOpts;
  }  
    
  // to handle error from backend requests
  public handleError(error: any){
    return Observable.throw(error);
  }

   // extract error messages from the body response
   public getErrorMessage(error: any){
    let errorMessage = ' Unable to process your request ';
    if ( error ){
      let errorMsg = error;
      try {
        errorMsg = error;
      } catch ( e ){

      }
      if ( errorMsg.error && errorMsg.error.message ){
        errorMessage = errorMsg.error.message;
      }
      return errorMessage;
    }
  }

    // extract error type from the body response
    public getErrorType(error: any){
      let errorMessage = ' Unable to process your request ';
      if ( error ) {
        let errorMsg = error;
        try {
          errorMsg = error;
        } catch ( e ) {
        }

        if (errorMsg.error && errorMsg.error.errorTypes){
          errorMessage = errorMsg.error.errorTypes;
        }
        return errorMessage;
      }
    }

}
